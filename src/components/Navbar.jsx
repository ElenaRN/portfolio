import React, { /* useState, useEffect  */} from "react";
import { Link } from "react-router-dom";
import BurguerMenu from "./BurguerMenu";

import "../styles/Navbar.scss";

const routes = [
  {
    name: "Home",
    to: "/",
   
  },
  {
    name: "Projects",
    to: "/projects",
    
   
  },
  {
    name: "AboutMe",
    to: "/about-me",
    
  },
 
];

export default function Navbar() {
  return (

    <div className="Navbar">
    <Link to="/"><img className="Navbar__logo" src="/assets/logo.png" alt="Logo"></img></Link>
        <div className="Navbar__menu">
        <Link  className="Navbar__link" to="/">Home</Link>
        <Link  className="Navbar__link" to="/projects">Mis Proyectos</Link>
        <Link  className="Navbar__link" to="/about-me">Sobre Mí</Link>
        </div>
        <BurguerMenu routes={routes}/>
    </div>
  );
}
