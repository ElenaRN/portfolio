import React, { useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import "../styles/BurguerMenu.scss";

export default function BurguerMenu(props) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const handleClick = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <div className="BurguerMenu">
      <button onClick={handleClick}><FontAwesomeIcon className="BurguerMenu__icon" icon={faBars} /></button>
      {isMenuOpen ? (
        <div className="BurguerMenu__list">
        {props.routes.map(({name, to}) =>(
          <p className="BurguerMenu__list__link" key={to}>
            <Link to={to}>{name}</Link>
          </p>
        ))}
         
        </div>
      ) : null}
    </div>
  );
}
