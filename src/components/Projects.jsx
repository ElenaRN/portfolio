import React from "react";
import { Fade } from "react-awesome-reveal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab} from "@fortawesome/free-brands-svg-icons";
import { faShareSquare} from "@fortawesome/free-solid-svg-icons";

import "../styles/Projects.scss";

export default function Projects(props) {
  return (
    <section className="Projects">
      <div className="Projects__container">
        {props.projects.map(({ title, description, video,netlify,gitlab }) => (
          <div className="Projects__container__list" key={title}>
          
            <div className="Projects__container__list--text">
           
              <h3>{title}</h3>
              <p>{description}</p>
              <div>
                <a className="Projects__container__list--text--netlify" href={netlify}><FontAwesomeIcon  icon={faShareSquare}/>Web</a>
                <a className="Projects__container__list--text--gitlab" href={gitlab}><FontAwesomeIcon  icon={faGitlab} /></a>
              </div>
            </div>
            <Fade direction="left">
            <div className="Projects__container__list--mp4">
           
              <video  controls title={title} src={video}></video>
            
            </div>
          </Fade>
          </div>
        ))}
      </div>
    </section>
  );
}
