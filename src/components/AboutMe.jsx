import React from "react";

import "../styles/AboutMe.scss";

export default function AboutMe(props) {
  console.log(props.about);
  return (
    <section className="AboutMe">
      {props.about.map((a) => (
        <div className="AboutMe__about" key={a.title}>

        <div className="AboutMe__about__left">
          <div className="AboutMe__about__left--img">
            <img src={a.img} alt="Foto informal"></img>
          </div>

          <div className="AboutMe__about__left--cv">
            <a href={a.urlCv} download="CV Elena Rábano Neila">
              Descarga mi CV
            </a>
           <p>Puedes contactar conmigo en este correo {a.email}</p>
           <p>O a través de mi perfil de </p>
           <a href={a.linkedin}>Linkedin</a>

          </div>
        </div>

          <div className="AboutMe__about__card">
            <div className="AboutMe__about__card--text">
              <h3 className="AboutMe__about__card--text__title">{a.title}</h3>
              <p className="AboutMe__about__card--text__border"></p>
              <p className="AboutMe__about__card--text__description">
                {a.description}
              </p>
            </div>

            <div className="AboutMe__about__card--tecnologies">
              {a.tecnologies.map((tecnology) => {
                return (
                  <div
                    className="AboutMe__about__card--tecnologies--tecnology"
                    key={tecnology}
                  >
                    <p>{tecnology}</p>
                  </div>
                );
              })}
            </div>
            
          </div>
        </div>
      ))}
    </section>
  );
}
