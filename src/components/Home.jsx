import React from 'react';


import '../styles/Home.scss';

export default function Home() {
    return (
        <section className="Home">
            <h1 className="Home__title">Elena Rábano Neila</h1>
            <h4 className="Home__subtitle">Front end Developer</h4>
            <p className="Home__border"></p>
            <p className="Home__description">"Tener la oportunidad de trabajar como FRONT-END es mi objetivo principal, usando diferentes tecnologías,
            con las mejores prácticas y un continuo aprendizaje, seguiré creciendo."</p>

           
            
        </section>
    )
}
