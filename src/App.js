import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar";

import Home from "./components/Home";
import Projects from "./components/Projects";
import AboutMe from "./components/AboutMe";

import jsonProjects from "./projects.json";
import jsonAbout from "./about.json";

import "./App.scss";

function App() {
  const [projects] = useState(jsonProjects);
  const [about] = useState(jsonAbout);

  return (
    <div className="App">
      <Navbar />
     

      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route
          exact
          path="/about-me"
          render={() => <AboutMe about={about} />}
        />
        <Route
          exact
          path="/projects"
          render={() => <Projects projects={projects} />}
        />
      </Switch>
    </div>
  );
}

export default App;
